.. index:: commands

.. _commands:

.. currentmodule:: yombo.lib.commands

=============================
Commands (yombo.lib.commands)
=============================
.. automodule:: yombo.lib.commands

Commands class
=============================
.. autoclass:: Commands
   :members:

   .. automethod:: __init__

Command class
=============================
.. autoclass:: Command
   :members:

   .. automethod:: __init__

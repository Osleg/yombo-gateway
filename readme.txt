=============
Yombo Gateway
=============

The Yombo Gateway is an open-source framework to provide automation of various
devices (X10, Z-Wave, Insteon, etc) from any manufacturer.  Yombo provides a
way out of vendor lock-in by allowing cross-vendor and cross-protocol bridging.

================
Installation
================

For detailed installation directions, visit:
http://www.yombo.net/docs/gateway/current/chapters/install.html

==========
Resources
==========

For issue (tickets), feature requests, and wiki articles, visit
`Yombo Gateway Projects <https://projects.yombo.net/projects/gateway>`_ page.

Track issues, wiki

=========================
Yombo Gateway License 
=========================

By accessing Yombo code, you are agreeing to the following licensing terms. 
If you do not agree to these terms, do not access the code.

Your license to the Yombo Gateway source and/or binaries is governed by the
Reciprocal Public License 1.5 license as described here in the included
LICENSE file distributed with this software.

If you do not wish to release the source of software you build using Yombo
Gateway, you may use Yombo Gateway source and/or binaries under the Yombo
Gateway End User License Agreement as described here:

http://www.yombo.net/policies/licensing/gateway_private

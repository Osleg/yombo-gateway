Logwriter Example
==================

Subscribes to all message distributions (status, command) and
saves any messages it sees to a log file.

Installation
============

Simply mark this module as being used by the gateway, and the gateway will
download this module and install this module automatically.

Requirements
============

None

License
=======

Same license as Yombo Gateway.


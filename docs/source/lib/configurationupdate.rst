.. index:: configurationupdate

.. _configuration_update:

.. currentmodule:: yombo.lib.configurationupdate

=====================================================
Configuration Update (yombo.lib.configurationupdate)
=====================================================
.. automodule:: yombo.lib.configurationupdate

ConfigurationUpdate Class
===================================
.. autoclass:: ConfigurationUpdate
   :members:

   .. automethod:: __init__
